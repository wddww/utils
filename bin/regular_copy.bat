@ECHO OFF
@REM 判断该文件是否存在
SET SourceFile=%ROOT_PATH%bin\java\search-files.jar
if exist %SourceFile% (
    echo 正在执行 java -Dfile.encoding=UTF-8 -jar  %SourceFile%
    java -Dfile.encoding=UTF-8 -jar  %SourceFile% regular_search_copy_file_to_zip
) else (
    echo You cannot use this feature, %SourceFile% is not exist!
)