@ECHO OFF
@REM 判断该文件是否存在
SET SourceFile=%ROOT_PATH%bin\java\search-files.jar
if exist %SourceFile% (
    echo 正在执行 java -Dfile.encoding=UTF-8 -jar  %SourceFile%
    java -Dfile.encoding=UTF-8 -jar  %SourceFile% img_batch_compress
) else (
    echo You cannot use this feature, %SourceFile% is not exist!
)