@ECHO OFF
@REM 判断该文件是否存在
SET SourceFile=%ROOT_PATH%bin\python\test.py
if exist %SourceFile% (
    echo 正在执行 python  %SourceFile%
    python  %SourceFile%
) else (
    echo You cannot use this feature, %SourceFile% is not exist!
)