package com.my.tool.utils.search.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName TipEntity
 * @Description TODO 功能描述类
 * @Author wangdong_02
 * Date 2022/3/10 17:08
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TipEntity {
    /**
     * id 唯一标识
     */
    String id;
    /**
     * 功能名称
     */
    String name;
    /**
     * 功能类型
     */
    String type;

}
