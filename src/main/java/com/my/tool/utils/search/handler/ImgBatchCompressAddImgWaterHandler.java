package com.my.tool.utils.search.handler;

import com.my.tool.utils.search.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ImgBatchCompressAddImgWaterHandler implements Handler {

    final String  FUNCTION_NAME = "add-img-water";

    @Autowired
    private ClientService clientService;

    @Override
    public void deal(Chain chain) {
        String type = chain.request();
        if (FUNCTION_NAME.equals(type)){
            clientService.imgBatchCompressAddImgWater();
        }else {
            chain.proceed(type);
        }
    }
}
