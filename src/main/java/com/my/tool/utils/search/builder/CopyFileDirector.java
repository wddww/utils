package com.my.tool.utils.search.builder;

public class CopyFileDirector {

    CopyFileBuilder copyFileBuilder = null;

    public CopyFileDirector(CopyFileBuilder copyFileBuilder) {
        this.copyFileBuilder = copyFileBuilder;
    }

    public void construct(){
        copyFileBuilder.creatFolder();
        copyFileBuilder.removeFiles();
        copyFileBuilder.getResult();
    }
}
