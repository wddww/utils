package com.my.tool.utils.search.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SearchConstants  {

    ZIP(0, "zip"),

    COPY(1, "copy");

    private int code;
    private String msg;

}
