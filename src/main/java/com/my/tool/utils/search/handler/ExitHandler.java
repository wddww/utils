package com.my.tool.utils.search.handler;


import org.springframework.stereotype.Component;

/**
 * @ClassName ExitHandler
 * @Description TODO 系统退出
 * @Author wangdong_02
 * Date 2022/3/10 17:17
 * @Version 1.0
 **/
@Component
public class ExitHandler implements Handler {

    final String  FUNCTION_NAME = "exit";

    @Override
    public void deal(Handler.Chain chain) {
        String type = chain.request();
        if (FUNCTION_NAME.equals(type)){
           System.exit(0);
        }else {
            chain.proceed(type);
        }
    }
}
