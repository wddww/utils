package com.my.tool.utils.search.properties;

import com.my.tool.utils.search.utils.FileUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Component
public
class SearchProperties extends  Properties {

    @Value("${prop.copy.url}")
    String  copyPath;

    @Value("${prop.current-path}")
    String  currentPath;

    @Value("${prop.resource-path}")
    String  resourcePath;

    @Value("${prop.search-path}")
    String  searchPath;


    public String copyToPath(){
        String defaultpath =StringUtils.isNotBlank( this.copyPath) ?
                this.getDefaulturl()
                : FileUtil.getParentDir(this.getDefaultlevel());
        return defaultpath;
    }

}
