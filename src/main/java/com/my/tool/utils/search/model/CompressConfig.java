package com.my.tool.utils.search.model;

import com.my.tool.utils.search.properties.Properties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CompressConfig extends Properties {
    /**
     * jar包文件所在的路径
     */
    String currentPath;

    /**
     * 查找到的文件保存的路径
     */
    String copyPath;

    /**
     * 查找的资源库
     */
    String resourcePath;

    /**
     * 正则表达式
     */
    String regular;

    /**
     * 查找到文件后输出的结果 压缩包或不压缩
     */
    String size;

    /**
     * 使用的功能
     */
    String fun;


    /**
     * 水印的文字
     */
    String  waterContent;

    /**
     * 水印的图片的路径
     */
    String  waterPath;


    int  x;

    int  x1;

    int  y;

    int  y2;

    int  w;

    int  h;

    float  opacity;

    float  quality;

    String  positions;

}
