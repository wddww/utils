package com.my.tool.utils.search.builder;

import com.my.tool.utils.search.model.CompressConfig;
import com.my.tool.utils.search.utils.FileToZip;
import com.my.tool.utils.search.utils.FileUtil;
import com.my.tool.utils.search.utils.ThumbnailatorUtil;
import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.geometry.Positions;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;

@Component
@Slf4j
public
class ImgBatchCompressAddImgExWaterResult extends CopyFileBuilder {

    @Autowired
    CompressConfig config;

    @Autowired
    private FileUtil fileUtil;

    List <File> resource;



    /**
     * 从哪个文件夹下开始查找
     * @return 返回查找到的结果
     */
    @Override
    public  List <File>  getResource(){
        return fileUtil.getFileList( config.getResourcePath(), false );
    }

    @Override
    public List <File>  searchFile() {
        this.resource = getResource();
        String path = config.getCopyPath();
        String pattern = config.getRegular();
        System.out.println("There are " + this.resource.size() +" resource pools");
        List<File> differList = new ArrayList<>();
        long beginTime = System.currentTimeMillis();
        StringBuffer sb = new StringBuffer(100);
        int num = 1;
        for (File file : this.resource) {
            sb.append(file.getPath());
            if (Pattern.matches(pattern,sb.toString()))
            {
                addWatermark(config.getWaterPath(),file.getPath(),config.getW(),config.getH(),path);
                differList.add(file);
                num++;
            }
            sb.delete(0,sb.length());
        }
        traverse(differList);
        long endTime = System.currentTimeMillis();
        log.info("Nested collection traversal takes " + (endTime - beginTime) +" milliseconds");
        return differList;
    }

    public void  addWatermark(String pressImg, String targetImg, int width, int height,String path) {
        try {
            //原图片流
            InputStream input = new FileInputStream(targetImg);
            //改变原图大小
//            //暂存加水印图片流
//            ByteArrayOutputStream output = new ByteArrayOutputStream();
//            //水印图片流
//            InputStream watermarkImageStream = new FileInputStream(pressImg);
//            //加水印，水印在右上角，到两个边的距离都为40像素，完全不透明，不旋转
//            // int resWidth = width - ImageIO.read(watermarkImageStream).getWidth() - 40;
//            int resWidth = ImageIO.read(watermarkImageStream).getWidth() - 40;
//            WaterMaskImgUtils.addWatermark(input, watermarkImageStream, output, resWidth, 40, 1F, 0);
//            // 复制新的图片流到指定目录下
//            //替换原图片流
//            input = new ByteArrayInputStream(output.toByteArray());

            copy(input,path);

        } catch (Exception e) {
            log.info("添加水印失败", e);
        }
    }

    private  void copy(InputStream src, String target) {
        Random rd = new Random(1);
        File targetFile = new File(target+File.separator+rd.toString()+".jpg");

        InputStream in = null;
        OutputStream out = null;
        try {
            in = src;
            out = new FileOutputStream(targetFile);

            byte[] b = new byte[1024];
            int len = -1;
            while ((len = in.read(b)) != -1) {
                out.write(b, 0, len);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (out!= null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void getResult() {
        searchFile();
    }

    @Override
    public void creatFolder() {
        try{
            File file = new File( this.config.getCopyPath());
            if(fileUtil.checkFolderExist(file)){
                file.mkdir();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void removeFiles() {
        try{
            File file = new File( this.config.getCopyPath() );
            if(fileUtil.checkFileExist(file)){
                fileUtil.deleteFile( file );
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    /**
     * 遍历集合,打印出每个元素
     *
     * @param list List集合
     */
    private  void traverse(List<File> list) {
        for (File str : list) {
            log.info("Matched file:" + str.getPath());
        }
    }
}
