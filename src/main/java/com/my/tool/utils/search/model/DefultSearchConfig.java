package com.my.tool.utils.search.model;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DefultSearchConfig {

    /**
     * jar包文件所在的路径
     */
    String currentPath;

    /**
     * 查找到的文件保存的路径
     */
    String copyPath;

    /**
     * 查找的资源库
     */
    String resourcePath;


    /**
     * 要查找的文件名称的文件
     */
    String searchPath;

}
