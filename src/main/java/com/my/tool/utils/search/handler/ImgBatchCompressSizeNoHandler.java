package com.my.tool.utils.search.handler;

import com.my.tool.utils.search.utils.FileUtil;
import com.my.tool.utils.search.utils.ThumbnailatorUtil;

public class ImgBatchCompressSizeNoHandler implements Handler {

    final String  FUNCTION_NAME = "compress-unchange";

    byte[] bytesByFile;

    String toPath = null;

    String newName = "";



    public
    ImgBatchCompressSizeNoHandler(byte[] bytesByFile, String toPath, String newName) {
        this.bytesByFile=bytesByFile;
        this.toPath=toPath;
        this.newName=newName;
    }

    @Override
    public void deal(Chain chain) {
        String type = chain.request();
        if (FUNCTION_NAME.equals(type)){
            byte[] bytes = ThumbnailatorUtil.compressImage(bytesByFile,"jpg",0.8f);
            FileUtil.getFileByBytes(bytes,toPath, newName);
        }else {
            chain.proceed(type);
        }
    }
}
