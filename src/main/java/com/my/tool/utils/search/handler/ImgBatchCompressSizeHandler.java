package com.my.tool.utils.search.handler;

import com.my.tool.utils.search.model.CompressConfig;
import com.my.tool.utils.search.service.ClientService;
import com.my.tool.utils.search.utils.FileToZip;
import com.my.tool.utils.search.utils.FileUtil;
import com.my.tool.utils.search.utils.ThumbnailatorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ImgBatchCompressSizeHandler implements Handler {

    final String  FUNCTION_NAME = "compress";

    @Autowired
    ClientService clientService;

    @Override
    public void deal(Chain chain) {
        String type = chain.request();
        if (FUNCTION_NAME.equals(type)){
            clientService.imgBatchCompressSize();
        }else {
            chain.proceed(type);
        }
    }
}
