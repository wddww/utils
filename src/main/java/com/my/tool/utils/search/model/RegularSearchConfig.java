package com.my.tool.utils.search.model;

import com.my.tool.utils.search.properties.Properties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RegularSearchConfig extends Properties {
    /**
     * jar包文件所在的路径
     */
    String currentPath;

    /**
     * 查找到的文件保存的路径
     */
    String copyRegularPath;

    /**
     * 查找的资源库
     */
    String resourceRegularPath;

    /**
     * 正则表达式
     */
    String regular;

    /**
     * 查找到文件后输出的结果 压缩包或不压缩
     */
    String typeRegular;
}
