package com.my.tool.utils.search.handler;

import com.my.tool.utils.search.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SearchCopyFileToDirHandler implements Handler {

    final String  FUNCTION_NAME = "search_copy_file_to_dir";

    @Autowired
    private ClientService clientService;

    @Override
    public void deal(Chain chain) {
        String type = chain.request();
        if (FUNCTION_NAME.equals(type)){
            clientService.copyFile();
        }else {
            chain.proceed(type);
        }
    }
}
