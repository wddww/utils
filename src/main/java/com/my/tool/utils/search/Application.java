package com.my.tool.utils.search;


import com.my.tool.utils.search.terminal.impl.SearchTerminal;

public class Application {

    public static void main(String[] args) {
        new SearchTerminal().run();
    }

}
