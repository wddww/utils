package com.my.tool.utils.search.handler;

import java.util.ArrayList;

public class ChainClient {
    private ArrayList<Handler> handlers;

    public ChainClient(ArrayList<Handler> handlers) {
        this.handlers = handlers;
    }

    public void addHandlers(Handler handler){
        handlers.add(handler);
    }

    public void execute(String request){
        ArrayList<Handler> handlerList = new ArrayList<>();
        handlerList.addAll(handlers);
        handlerList.add(new DefaultHandler());
        RealChain realChain = new RealChain(request,handlerList,0);
        realChain.proceed(request);
    }
}
