package com.my.tool.utils.search.service;

public
interface ClientService {

    /**
     * 从一个文件夹下查找想要的文件放置在一个新的文件夹里
     */
    void copyFile();

    /**
     * 检索正则表达式所搜的内容不做任何操作
     */
    void searchFile();

    /**
     * 根据正则表达式搜索文件压缩成ZIP
     */
    void copyFileRegular();

    /**
     * 根据正则表达式搜索文件直接复制文件
     */
    void copyFileByRegularToDir();


    /**
     * 批量压缩图片
     */
    void imageBatchCompress();


    /**
     * 添加水印
     */
    void imgBatchCompressAddTxtWater();

    /**
     * 添加图片水印
     */
    void imgBatchCompressAddImgWater();

    /**
     * 添加图片水印高清版
     */
    void imgBatchCompressAddImgExWater();

    /**
     * 剪切
     */
    void imgBatchCompressCut();

    /**
     * 尺寸
     */
    void imgBatchCompressSize();

}
