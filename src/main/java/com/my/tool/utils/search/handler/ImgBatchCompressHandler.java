package com.my.tool.utils.search.handler;

import com.my.tool.utils.search.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 图片批量压缩
 * @author wangdong_02
 * Date 2022/3/7
 */
@Component
public class ImgBatchCompressHandler implements Handler {

    final String  FUNCTION_NAME = "img_batch_compress";

    @Autowired
    private ClientService clientService;

    @Override
    public void deal(Chain chain) {
        String type = chain.request();
        if (FUNCTION_NAME.equals(type)){
            clientService.imageBatchCompress();
        }else {
            chain.proceed(type);
        }
    }
}
