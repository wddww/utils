package com.my.tool.utils.search.builder;

import com.my.tool.utils.search.model.RegularSearchConfig;
import com.my.tool.utils.search.utils.FileToZip;
import com.my.tool.utils.search.utils.FileUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@Component
@Slf4j
public class ReduceResult extends CopyFileBuilder {

    final String FILE_NAME = "压缩文件";

    @Autowired
    RegularSearchConfig config;

    @Autowired
    private FileUtil fileUtil;



    /**
     * 从哪个文件夹下开始查找
     * @return 返回查找到的结果
     */
    @Override
    public  List <File>  getResource(){
        return fileUtil.getFileList( config.getResourceRegularPath(), false );
    }

    @Override
    public List <File>  searchFile() {
        List <File> resource = getResource();
        String pattern = config.getRegular();  // 字符串中是否包含了 '原片' 子字符串
        log.info("There are " + resource.size() +" resource pools");
        List<File> differList = new ArrayList<>();
        long beginTime = System.currentTimeMillis();
        StringBuffer sb = new StringBuffer(100);
        for (File file : resource) {
            sb.append(file.getPath());
            if (Pattern.matches(pattern,sb.toString()))
            {
                differList.add(file);
            }
            sb.delete(0,sb.length());
        }
        traverse(differList);
        long endTime = System.currentTimeMillis();
        log.info("Nested collection traversal takes " + (endTime - beginTime) +" milliseconds");
        return differList;
    }


    /**
     * 遍历集合,打印出每个元素
     *
     * @param list List集合
     */
    private  void traverse(List<File> list) {
        for (File str : list) {
            log.info("Matched file:" + str.getPath());
        }
    }

    @Override
    public void getResult() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        LocalDateTime localDateTime = timestamp.toLocalDateTime();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss");
        FileToZip.fileToZip( searchFile().toArray(new File[]{}),config.getCopyRegularPath(),FILE_NAME + localDateTime.format(dateTimeFormatter));
    }

    @Override
    public void creatFolder() {
        try{
            File file = new File( this.config.getCopyRegularPath());
            if(fileUtil.checkFolderExist(file)){
                file.mkdir();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void removeFiles() {
        try{
            File file = new File( this.config.getCopyRegularPath() );
            if(fileUtil.checkFileExist(file)){
                fileUtil.deleteFile( file );
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
