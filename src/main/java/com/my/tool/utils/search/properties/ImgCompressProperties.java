package com.my.tool.utils.search.properties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Component
public
class ImgCompressProperties extends  Properties  {

    @Value("${prop.copy-img-path}")
    String  copyPath;

    @Value("${prop.current-img-path}")
    String  currentPath;

    @Value("${prop.resource-img-path}")
    String  resourcePath;

    @Value("${prop.search-img-regular}")
    String  regular;

    @Value("${prop.img-compress-size}")
    String  size;

    @Value("${prop.img-water-content}")
    String  waterContent;


    @Value("${prop.img-coordinate-x}")
    int  x;

    @Value("${prop.img-coordinate-x1}")
    int  x1;

    @Value("${prop.img-coordinate-y}")
    int  y;

    @Value("${prop.img-coordinate-y2}")
    int  y2;

    @Value("${prop.img-coordinate-w}")
    int  w;

    @Value("${prop.img-coordinate-h}")
    int  h;

    @Value("${prop.img-water-path}")
    String  waterPath;

    @Value("${prop.opacity}")
    float  opacity;

    @Value("${prop.quality}")
    float  quality;

    @Value("${prop.positions}")
    String  positions;



}
