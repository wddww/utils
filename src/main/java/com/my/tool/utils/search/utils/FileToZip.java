package com.my.tool.utils.search.utils;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * 将文件夹下面的文件
 * 打包成zip压缩文件
 *
 * @author admin
 *
 */
public class FileToZip {

    private FileToZip(){}


    /**
     * 将存放在sourceFilePath目录下的源文件，打包成fileName名称的zip文件，并存放到zipFilePath路径下
     * 可以有重名的文件
     * @param zipFilePath :压缩后存放路径
     * @param fileName :压缩后文件的名称
     * @return
     */
    public static boolean fileToZip(File[] sourceFiles,String zipFilePath,String fileName){
        boolean flag = false;
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        FileOutputStream fos = null;
        ZipOutputStream zos = null;
        try {
            File zipFile = new File(zipFilePath + File.separator + fileName +".zip");
            if(zipFile.exists()){
                System.out.println(zipFilePath + "目录下存在名字为:" + fileName +".zip" +"打包文件.");
            }else{
                fos = new FileOutputStream(zipFile);
                zos = new ZipOutputStream(new BufferedOutputStream(fos));
                byte[] bufs = new byte[1024*10];
                for(int i=0;i<sourceFiles.length;i++){
                    File sourceFile =  createOrRenameFile(sourceFiles[i], i);
                    //创建ZIP实体，并添加进压缩包
                    ZipEntry zipEntry = new ZipEntry(sourceFile.getName());
                    zos.putNextEntry(zipEntry);
                    //读取待压缩的文件并写进压缩包里
                    fis = new FileInputStream(sourceFiles[i]);
                    bis = new BufferedInputStream(fis, 1024*10);
                    int read = 0;
                    while((read=bis.read(bufs, 0, 1024*10)) != -1){
                        zos.write(bufs,0,read);
                    }
                }
                flag = true;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally{
            //关闭流
            try {
                if(null != bis) bis.close();
                if(null != zos) zos.close();
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
        return flag;
    }

    public static File createOrRenameFile(File from,int num){
        String[] fileInfo = getFileInfo(from);
        String toPrefix=fileInfo[0];
        String toSuffix=fileInfo[1];
        File file;
        file = createOrRenameFile(from, toPrefix + "_"+num, toSuffix);
        return file;
    }

    /**
     * 获取文件名和后缀名
     * @param from
     * fileInfo[0]: toPrefix;
     * fileInfo[1]:toSuffix;
     * @return
     */
    public static String[] getFileInfo(File from){
        String fileName=from.getName();
        int index = fileName.lastIndexOf(".");
        String toPrefix="";
        String toSuffix="";
        if(index==-1){
            toPrefix=fileName;
        }else{
            toPrefix=fileName.substring(0,index);
            toSuffix=fileName.substring(index,fileName.length());
        }
        return new String[]{toPrefix,toSuffix};
    }



    /**
     * sdcard/pic/tanyang.jpg
     * toPrefix: tanyang
     * toSuffix: tanyang.jpg
     * @param from
     * @param toPrefix
     * @param toSuffix
     * @return
     */

    public static File createOrRenameFile(File from, String toPrefix, String toSuffix) {
        File directory = from.getParentFile();
        return new File(directory, toPrefix + toSuffix);
    }

}
