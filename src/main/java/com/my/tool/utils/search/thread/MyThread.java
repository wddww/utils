package com.my.tool.utils.search.thread;

public class MyThread extends Thread {
    int j = 20;
    public void run(){
        for (int i=0;i<20;i++){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(this.getName()+",i="+j--);
        }
    }


    public static void main(String[] args) {
        MyThread myThread = new MyThread();
        Thread t1 = new Thread(myThread);
        Thread t2 = new Thread(myThread);
        t1.start();
        t2.start();
    }
}
