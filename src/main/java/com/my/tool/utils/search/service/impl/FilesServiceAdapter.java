package com.my.tool.utils.search.service.impl;

import com.my.tool.utils.search.model.DefultSearchConfig;
import com.my.tool.utils.search.service.FilesService;
import com.my.tool.utils.search.utils.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FilesServiceAdapter implements FilesService {

    @Autowired
    private DefultSearchConfig config;

    @Autowired
    private FileUtil fileUtil;


    /**
     * 查找的图片源
     * @return
     */
    @Override
    public
    List<String> getResource() {
        List <File> resource = fileUtil.getFileList( config.getResourcePath( ), false );
        return resource.stream( ).map( File::getPath ).collect( Collectors.toList( ) );
    }

    /**
     * 需要比对的图片源
     * @return
     */
    @Override
    public
    List <String> searchFiles() {
        File file = new File( config.getSearchPath());
        return fileUtil.readLins( file);
    }

    @Override
    public
    void copyFileToPath() {
        fileUtil.copyFileToPath(returnFindResult(), config.getCopyPath());
    }

    @Override
    public
    List <String> returnFindResult() {
        List <String> resource = getResource();
        List <String> searchFiles = searchFiles( );
        return fileUtil.compare( resource, searchFiles );
    }


    @Override
    public void creatFolder() {
        try{
            File file = new File( this.config.getCopyPath() );
            if(fileUtil.checkFolderExist(file)){
                file.mkdir();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void removeFiles() {
        try{
            File file = new File( this.config.getCopyPath() );
            if(fileUtil.checkFileExist(file)){
                fileUtil.deleteFile( file );
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
