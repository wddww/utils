package com.my.tool.utils.search.config;

import com.my.tool.utils.search.model.CompressConfig;
import com.my.tool.utils.search.properties.ImgCompressProperties;
import com.my.tool.utils.search.utils.FileUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.swing.plaf.metal.MetalIconFactory;
import java.io.File;

@Configuration
public class ImageCompressConfig {


    @Autowired
    ImgCompressProperties imgCompressProperties;

    @Bean
    public CompressConfig compressConfig() {
        return  CompressConfig.builder()
                .copyPath(imgCompressProperties.getCopyPath())
                .resourcePath(imgCompressProperties.getResourcePath())
                .currentPath(imgCompressProperties.getCurrentPath())
                .regular(imgCompressProperties.getRegular())
                .size(imgCompressProperties.getSize())
                .waterContent(imgCompressProperties.getWaterContent())
                .x( imgCompressProperties.getX() )
                .x1( imgCompressProperties.getX1())
                .y( imgCompressProperties.getY() )
                .y2( imgCompressProperties.getY2())
                .w(imgCompressProperties.getW())
                .h(imgCompressProperties.getH())
                .waterPath(imgCompressProperties.getWaterPath())
                .opacity(imgCompressProperties.getOpacity())
                .quality(imgCompressProperties.getQuality())
                .positions(imgCompressProperties.getPositions())
                .build();
    }




}
