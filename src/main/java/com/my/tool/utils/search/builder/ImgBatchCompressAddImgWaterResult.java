package com.my.tool.utils.search.builder;

import com.my.tool.utils.search.model.CompressConfig;
import com.my.tool.utils.search.utils.FileToZip;
import com.my.tool.utils.search.utils.FileUtil;
import com.my.tool.utils.search.utils.ThumbnailatorUtil;
import net.coobird.thumbnailator.geometry.Position;
import net.coobird.thumbnailator.geometry.Positions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@Component
public
class ImgBatchCompressAddImgWaterResult extends CopyFileBuilder {


    @Autowired
    CompressConfig config;

    @Autowired
    private FileUtil fileUtil;

    List <File> resource;




    /**
     * 从哪个文件夹下开始查找
     * @return 返回查找到的结果
     */
    @Override
    public  List <File>  getResource(){
        return fileUtil.getFileList( config.getResourcePath(), false );
    }

    @Override
    public List <File>  searchFile() {
        this.resource = getResource();
        String path = config.getCopyPath();
        String pattern = config.getRegular();
        System.out.println("There are " + this.resource.size() +" resource pools");
        List<File> differList = new ArrayList<>();
        long beginTime = System.currentTimeMillis();
        StringBuffer sb = new StringBuffer(100);
        int num = 1;
        for (File file : this.resource) {
            sb.append(file.getPath());
            if (Pattern.matches(pattern,sb.toString()))
            {
                byte[] bytesByFile = FileUtil.getBytesByFile(file);
                String name=FileToZip.createOrRenameFile( file, num ).getName( );
                byte[] bytes =ThumbnailatorUtil.addImageWater( bytesByFile, config.getW(),config.getH(), Positions.valueOf(config.getPositions()),config.getWaterPath(),config.getOpacity(),config.getQuality());
                FileUtil.getFileByBytes(bytes,path, name);
                differList.add(file);
                num++;
            }
            sb.delete(0,sb.length());
        }
        traverse(differList);
        long endTime = System.currentTimeMillis();
        System.out.println("Nested collection traversal takes " + (endTime - beginTime) +" milliseconds");
        return differList;
    }

    @Override
    public void getResult() {
        searchFile();
    }

    @Override
    public void creatFolder() {
        try{
            File file = new File( this.config.getCopyPath());
            if(fileUtil.checkFolderExist(file)){
                file.mkdir();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void removeFiles() {
        try{
            File file = new File( this.config.getCopyPath() );
            if(fileUtil.checkFileExist(file)){
                fileUtil.deleteFile( file );
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    /**
     * 遍历集合,打印出每个元素
     *
     * @param list List集合
     */
    private  void traverse(List<File> list) {
        for (File str : list) {
            System.out.println("Matched file:" + str.getPath());
        }
    }
}
