package com.my.tool.utils.search.utils;

import com.my.tool.utils.search.Application;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

@Component
@Slf4j
@Data
public class FileUtil {

    final int STRING_BUFFER_SIZE = 200;

    final String CHARACTER="UTF-8";

    int sort = 0;

    int all = 0;

    /**
     * 如果文件夹不存在,就创建否则就不创建
     * @param file
     * @return
     */
    public  Boolean checkFolderExist(File file){
        return !file.exists();
    }

    /**
     * 检查File是否是文件
     * @param file
     * @return
     */
    public  Boolean checkFileExist(File file){
        return file != null && file.isDirectory();
    }

    /**
     * 删除文件夹
     * @param file
     * @return
     */
    public  void deleteFile(File file){
        try {
            FileUtils.cleanDirectory( file );
        } catch (IOException e) {
            e.printStackTrace( );
        }
    }


    /**
     * 复制文件到指定目录
     * @param file
     * @return
     */
    public  void copyFile(File file,File toFile) {
        try {
            FileUtils.copyFile(file,toFile);
        } catch (IOException e) {
            e.printStackTrace( );
        }
    }

    public
    List<String> readLins(File file){
        return readLins(file,CHARACTER);
    }

    public List<String> readLins(File file,String character){
        try {
            return FileUtils.readLines( file, character );
        } catch (IOException e) {
            e.printStackTrace( );
        }
        return null;
    }


    /**
     * 获取指定文件夹下的所有文件和文件夹
     *
     * @param folder
     * @param includeFolder  是否包含文件夹
     * @return
     */
    public  List<File> getFileList(String folder, boolean includeFolder) {
        class GetFileList {
            private List<File> files = new LinkedList<>();
            public List<File> getFile(String path) {
                File file = new File(path);
                File[] array = file.listFiles();
                try{
                    for (int i = 0; i < array.length; i++) {
                        if (array[i].isFile()) {
                            files.add(array[i]);
                        } else if (array[i].isDirectory()) {
                            if (includeFolder) {
                                files.add(array[i]);
                            }
                            getFile(array[i].getPath());
                        }
                    }
                }catch (Exception e){
                    log.error("路径："+ path + "错误：该路径不存在或不存在文件！");
                }
                return files;
            }
        }
        return new GetFileList().getFile(folder);
    }






    /**
     * 复制文件到指定的路径下
     * @param list   文件名称集合
     * @param targetPath  目标路径
     */
    public void   copyFileToPath(List <String> list,String targetPath){
        StringBuffer sb = new StringBuffer( STRING_BUFFER_SIZE  );
        list.stream().forEach( item->{
            sb.delete( 0, sb.length());
            sb.append(item);
            File file = new File(sb.toString());
            String fileName = sb.substring(sb.lastIndexOf(File.separator )+1, sb.length());
            File tofile = new File( targetPath + File.separator + fileName);
            copyFile( file ,tofile);
        });
    }

    public  List<String> compare(List<String> listA, List<String> listB){
       return getDifferListByNest(listA,listB);
    }

    /**
     * 借助借用来获取listA、listB的相似同元素集合
     *
     * @param listA 集合A
     * @param listB 集合B
     * @return list<String>
     */
    private  List<String> getDifferListByNest(List<String> listA, List<String> listB) {
        log.info("There are " + listA.size() +" resource pools");
        List<String> differList = new ArrayList<>();
        long beginTime = System.currentTimeMillis();
        for (String strB : listB) {
            for (String strA : listA) {
                if (StringUtils.isNotBlank( strB ) && strA.contains( strB ))
                {
                    differList.add(strA);
                }
            }
        }
        traverse(differList);
        long endTime = System.currentTimeMillis();
        log.info("Nested collection traversal takes " + (endTime - beginTime) +" milliseconds");
        return differList;
    }

    /**
     * 遍历集合,打印出每个元素
     *
     * @param list List集合
     */
    private  void traverse(List<String> list) {
        for (String str : list) {
            log.info("Matched file:" + str);
        }
    }

    /**
     * 将文件转换成Byte数组
     * @param file
     * @return
     */
    public static byte[] getBytesByFile(File file) {
        try {
            FileInputStream fis = new FileInputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream(1000);
            byte[] b = new byte[1000];
            int n;
            while ((n = fis.read(b)) != -1) {
                bos.write(b, 0, n);
            }
            fis.close();
            byte[] data = bos.toByteArray();
            bos.close();
            return data;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    //将Byte数组转换成文件
    public static void getFileByBytes(byte[] bytes, String filePath, String fileName) {
        BufferedOutputStream bos = null;
        FileOutputStream fos = null;
        File file = null;
        try {
            File dir = new File(filePath);
            if (!dir.exists()) {// 判断文件目录是否存在
                dir.mkdirs();
            }
            file = new File(filePath + File.separator + fileName);
            fos = new FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            bos.write(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 获取当前类文件的绝对路径
     * @return
     */
    public static String getConcurrentDir(){
        String jarWholePath = Application.class.getProtectionDomain().getCodeSource().getLocation().getFile();
        try {
            //保险起见，将路径进行decode转码
            jarWholePath = java.net.URLDecoder.decode(jarWholePath, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.info(e.toString());
        }
        //获取jar包的上级目录
        String jarPath = new File(jarWholePath).getAbsolutePath() + File.separator;
        log.info( "获取jar包的上级目录:"+ jarPath);
        return jarPath;
    }


    /**
     * 获取当前类文件的绝对路径前几级的方法
     * @return
     */
    public static String getParentDir(int parentlevel){
        String  path =  getConcurrentDir();
        Map<String, Integer> levelDir=getLevelDir( path, File.separatorChar );
        Integer count=levelDir.size();
        if (count > 0){
            int reallevel = count - parentlevel;
            path=path.substring( 0, levelDir.get( String.valueOf( reallevel ) ) );
        }
        return  path;
    }


    /**
     * 获取当前类文件的绝对路径前几级的方法
     * @return
     */
    public static Map<String,Integer> getLevelDir(String  content, char searchChar){
        Map<String,Integer> indexs = new ConcurrentHashMap<>();
        int count = 0;
        if (content.indexOf( searchChar ) > -1){
            char[] arrContent = content.toCharArray();
            for (int i = 0; i < arrContent.length; i++)
            {
                if (searchChar == arrContent[i])
                {
                    count++;
                    indexs.put(String.valueOf( count )  ,i );
                }
            }
        }
        return indexs;
    }


    public  void findFileByRegularAndCopyFile(String resourcePath,String pattern,int all, int sort,String targetPath) {
        findFileByRegular(resourcePath,pattern,all,sort,targetPath,true);
    }

    public void findFileByRegularAndNoCopyFile(String resourcePath,String pattern,int all, int sort,String targetPath) {
        findFileByRegular(resourcePath,pattern,all,sort,targetPath,false);
    }


    /**
     *
     * @param resourcePath  资源库
     * @param pattern       正则表达式
     * @param all           初始化值 默认为0
     * @param sort          初始化值 默认为0
     * @param targetPath    存入查找结果的路径
     * @param isCopyFile    是否复制文件 仅打印在控制台或复制文件
     */
    public  void findFileByRegular(String resourcePath,String pattern,int all, int sort,String targetPath,Boolean isCopyFile) {
        File file = new File(resourcePath);
        File[] array = file.listFiles();
        try{
            for (int i = 0; i < array.length; i++) {
                if (array[i].isFile()) {
                    if (Pattern.matches(pattern,array[i].getPath()))
                    {
                        log.info("Matched file:" + array[i].getPath());
                        if (isCopyFile){
                            File tofile = new File( targetPath + File.separator + array[i].getName());
                            File renameFile = FileToZip.createOrRenameFile(tofile, sort);
                            copyFile(array[i],renameFile);
                        }
                        this.sort++;
                    }
                    this.all++;
                } else if (array[i].isDirectory()) {
                    findFileByRegular(array[i].getPath(),pattern,this.all,this.sort,targetPath,isCopyFile);
                }
            }
        }catch (Exception e){
            log.error("路径："+ resourcePath + "错误：该路径不存在或不存在文件！");
        }
    }




}
