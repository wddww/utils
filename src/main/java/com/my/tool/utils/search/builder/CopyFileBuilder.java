package com.my.tool.utils.search.builder;


import java.io.File;
import java.util.List;


public abstract class CopyFileBuilder {

    /**
     * 资源库
     */
    public abstract  List <File>  getResource();

    /**
     * 根据正则表达式查找想要的结果
     * @return 返回查找到的结果
     */
    public abstract List <File>  searchFile();

    /**
     * 最终构建出来的结果
     */
    public abstract void getResult();

    /**
     * 拷贝文件的操作步骤
     * 创建保存文件的地方
     */
    public abstract void creatFolder();

    /**
     * 清空文件夹下的文件
     */
    public abstract   void removeFiles();



}
