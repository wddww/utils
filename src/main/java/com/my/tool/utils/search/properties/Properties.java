package com.my.tool.utils.search.properties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Component
public  class Properties {
    @Value("${prop.defult.url}")
    String defaulturl;

    @Value("${prop.defult.level}")
    int defaultlevel;
}
