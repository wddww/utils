package com.my.tool.utils.search.config;

import com.my.tool.utils.search.model.DefultSearchConfig;
import com.my.tool.utils.search.model.RegularSearchConfig;
import com.my.tool.utils.search.properties.SearchProperties;
import com.my.tool.utils.search.utils.FileUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;

@Configuration
public class SearchConfig {


    @Autowired
    SearchProperties searchProperties;

    @Bean
    public DefultSearchConfig defultSearchConfig(){

        return  DefultSearchConfig.builder()
                .copyPath(searchProperties.getCopyPath())
                .resourcePath(searchProperties.getResourcePath())
                .currentPath(searchProperties.getCurrentPath())
                .searchPath(searchProperties.getSearchPath())
                .build();
    }




}
