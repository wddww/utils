package com.my.tool.utils.search.builder;

import com.my.tool.utils.search.Application;
import com.my.tool.utils.search.model.RegularSearchConfig;
import com.my.tool.utils.search.utils.FileToZip;
import com.my.tool.utils.search.utils.FileUtil;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.List;
import java.util.regex.Pattern;

@Component
@Slf4j
public class SearchResult extends CopyFileBuilder {

    @Autowired
    RegularSearchConfig config;

    @Autowired
    private FileUtil fileUtil;

    /**
     * 从哪个文件夹下开始查找
     * @return 返回查找到的结果
     */
    @Override
    public  List <File>  getResource(){
        return fileUtil.getFileList( config.getResourceRegularPath(), false );
    }

    @Override
    public List <File>  searchFile() {
        return null;
    }


    @Override
    public void getResult() {
        String pattern = config.getRegular();
        long beginTime = System.currentTimeMillis();
        fileUtil.findFileByRegularAndNoCopyFile(config.getResourceRegularPath(),pattern,0,0,config.getCopyRegularPath());
        long endTime = System.currentTimeMillis();
        log.info("There are " + fileUtil.getAll() +" resource pools and target file find : " + fileUtil.getSort() + ", consuming time " + (endTime - beginTime) / 1000 + " Ms. ");
    }



    @Override
    public void creatFolder() {

    }

    @Override
    public void removeFiles() {

    }
}
