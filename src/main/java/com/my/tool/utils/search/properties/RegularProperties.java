package com.my.tool.utils.search.properties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Component
public
class RegularProperties extends  Properties {

    @Value("${prop.copy-regular-path}")
    String  copyPath;

    @Value("${prop.current-regular-path}")
    String  currentPath;

    @Value("${prop.resource-regular-path}")
    String  resourcePath;

    @Value("${prop.search-regular}")
    String  regular;

}
