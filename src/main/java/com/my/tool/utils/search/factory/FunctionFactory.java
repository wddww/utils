package com.my.tool.utils.search.factory;

import com.my.tool.utils.search.handler.*;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * 静态工厂模式 功能工厂
 */
@Component
@Setter
public class FunctionFactory {

    private String type;

    @Autowired
    ImgBatchCompressHandler imgBatchCompressHandler;

    @Autowired
    RegularSearchCopyFileToDirHandler regularSearchCopyFileToDirHandler;

    @Autowired
    RegularSearchCopyFileToZipHandler regularSearchCopyFileToZipHandler;

    @Autowired
    SearchCopyFileToDirHandler searchCopyFileToDirHandler;

    @Autowired
    SearchFileHandler searchFileHandler;

    @Autowired
    ImgBatchCompressSizeHandler imgBatchCompressSizeHandler;

    @Autowired
    ImgBatchCompressCutHandler imgBatchCompressCutHandler;

    @Autowired
    ImgBatchCompressAddTxtWaterHandler imgBatchCompressAddTxtWaterHandler;

    @Autowired
    ImgBatchCompressAddImgWaterHandler imgBatchCompressAddImgWaterHandler;

    @Autowired
    ImgBatchCompressAddImgExWaterHandler imgBatchCompressAddImgExWaterHandler;

    @Autowired
    ExitHandler exitHandler;

    public void execute(){
        ArrayList<Handler> handlerList = new ArrayList<>();
        handlerList.add(imgBatchCompressHandler);
        handlerList.add(regularSearchCopyFileToDirHandler);
        handlerList.add(regularSearchCopyFileToZipHandler);
        handlerList.add(searchCopyFileToDirHandler);
        handlerList.add(searchFileHandler);
        handlerList.add(imgBatchCompressSizeHandler);
        handlerList.add(imgBatchCompressCutHandler);
        handlerList.add(imgBatchCompressAddTxtWaterHandler);
        handlerList.add(imgBatchCompressAddImgWaterHandler);
        handlerList.add(imgBatchCompressAddImgExWaterHandler);
        handlerList.add(exitHandler);
        ChainClient chainClient = new ChainClient(handlerList);
        chainClient.execute(type);
    }
}
