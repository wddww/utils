package com.my.tool.utils.search.terminal.impl;

import com.my.tool.utils.search.factory.FunctionFactory;
import com.my.tool.utils.search.model.TipEntity;
import com.my.tool.utils.search.terminal.Terminal;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * @ClassName SearchTerminal
 * @Description TODO 查询图片的工具类
 * @Author wangdong_02
 * Date 2022/3/10 15:10
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
public class SearchTerminal implements Terminal {

    final static   String CONFIG_LOCATION =  "application-context.xml";

    ApplicationContext context = new ClassPathXmlApplicationContext(CONFIG_LOCATION);

    Map<String,TipEntity> functions = null;

    public void getFunction(String type){
        FunctionFactory functionFactory =this.context.getBean( FunctionFactory.class );
        functionFactory.setType(type);
        functionFactory.execute();
    }

    public void printTip(){
        this.functions = new LinkedHashMap<>();
        this.functions.put("1",new TipEntity("1","原图添加水印","add-img-water"));
        this.functions.put("2",new TipEntity("2","根据正则表达式查询文件名","add-img-water"));
        this.functions.put("0",new TipEntity("0","退出","exit"));
        formatOutput();
    }

    public void formatOutput(){
        System.out.println("========================");
        for (Map.Entry<String,TipEntity> entry : functions.entrySet()) {
            System.out.println("|-"+entry.getKey()+"."+entry.getValue().getName());
        }
        System.out.println("========================");
        System.out.println("请输入功能序号：");
    }

    public void run(){
        printTip();
        Scanner scanner = new Scanner(System.in);
        String str = scanner.next();
        getFunction(this.functions.get(str).getType());
        run();
    }

}
