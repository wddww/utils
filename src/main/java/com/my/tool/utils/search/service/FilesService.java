package com.my.tool.utils.search.service;

import java.util.List;

/**
 * 从一个文件夹下查找想要的文件放置在一个新的文件夹里
 *@Author Wangd
 *@Date Created in 2021/3/30 10:45
 */
public interface FilesService {
    /**
     * 创建文件夹
     */
    void creatFolder();

    /**
     * 批量删除文件夹下的文件
     */
    void removeFiles();


    List<String> getResource();

    List <String> searchFiles();

    List <String> returnFindResult();

    void copyFileToPath();


    /**
     * 初始化文件夹
     */
    default void initFolder(){
        creatFolder();
        removeFiles();
    }


    /**
     * 输出结果
     */
    default void outputResult(){
        initFolder();
        copyFileToPath();
    }
}
