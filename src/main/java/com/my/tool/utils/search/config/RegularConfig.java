package com.my.tool.utils.search.config;

import com.my.tool.utils.search.model.RegularSearchConfig;
import com.my.tool.utils.search.properties.RegularProperties;
import com.my.tool.utils.search.utils.FileUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;

@Configuration
public class RegularConfig {


    @Autowired
    RegularProperties regularProperties;

    @Bean
    public RegularSearchConfig regularSearchConfig(){

        return  RegularSearchConfig.builder()
                .copyRegularPath(regularProperties.getCopyPath())
                .resourceRegularPath(regularProperties.getResourcePath())
                .regular(regularProperties.getRegular())
                .currentPath(regularProperties.getCurrentPath())
                .build();
    }


}
