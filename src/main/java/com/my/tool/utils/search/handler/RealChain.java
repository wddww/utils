package com.my.tool.utils.search.handler;

import java.util.List;

public class RealChain implements Handler.Chain {

    public String request;

    public List<Handler> handlers;

    public int index;

    public RealChain(String request, List<Handler> handlers, int index) {
        this.request = request;
        this.handlers = handlers;
        this.index = index;
    }

    @Override
    public String request() {
        return request;
    }

    @Override
    public void proceed(String request) {
          if (handlers.size()> index){
              RealChain realChain = new RealChain(request,handlers,index +1);
              Handler handler = handlers.get(index);
              handler.deal(realChain);
          }
    }
}
