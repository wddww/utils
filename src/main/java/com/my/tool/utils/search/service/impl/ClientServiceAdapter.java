package com.my.tool.utils.search.service.impl;

import com.my.tool.utils.search.builder.*;
import com.my.tool.utils.search.service.FilesService;
import com.my.tool.utils.search.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public
class ClientServiceAdapter implements ClientService {

    @Autowired
    private FilesService filesService;

    @Autowired
    ReduceResult reduceResult;

    @Autowired
    RemoveResult removeResult;

    @Autowired
    ImageBatchCompressResult imageBatchCompressResult;


    @Autowired
    SearchResult searchResult;

    @Autowired
    ImgBatchCompressAddTxtWaterResult imgBatchCompressAddTxtWaterResult;

    @Autowired
    ImageBatchCompressCutResult imageBatchCompressCutResult;

    @Autowired
    ImgBatchCompressAddImgWaterResult imgBatchCompressAddImgWaterResult;

    @Autowired
    ImgBatchCompressAddImgExWaterResult imgBatchCompressAddImgExWaterResult;




    /**
     * 批量拷贝文件（流程和具体操作融合在一起了不遵循高内聚低耦合）
     */
    @Override
    public void copyFile() {
        filesService.outputResult();
    }

    @Override
    public
    void searchFile() {
        CopyFileDirector copyFileDirector = new CopyFileDirector(searchResult);
        copyFileDirector.construct();
    }

    /**
     *批量拷贝文件（流程和具体操作分开，遵循高内聚低耦合）使用的是建造者模式和责任链模式
     */
    @Override
    public void copyFileRegular() {
        CopyFileDirector copyFileDirector = new CopyFileDirector(reduceResult);
        copyFileDirector.construct();
    }

    @Override
    public void copyFileByRegularToDir() {
        CopyFileDirector copyFileDirector = new CopyFileDirector(removeResult);
        copyFileDirector.construct();
    }

    @Override
    public void imageBatchCompress() {
        CopyFileDirector copyFileDirector = new CopyFileDirector(imageBatchCompressResult);
        copyFileDirector.construct();
    }

    @Override
    public
    void imgBatchCompressAddTxtWater() {
        CopyFileDirector copyFileDirector = new CopyFileDirector(imgBatchCompressAddTxtWaterResult);
        copyFileDirector.construct();
    }

    @Override
    public void imgBatchCompressAddImgWater() {
        CopyFileDirector copyFileDirector = new CopyFileDirector(imgBatchCompressAddImgWaterResult);
        copyFileDirector.construct();
    }

    @Override
    public void imgBatchCompressAddImgExWater() {
        CopyFileDirector copyFileDirector = new CopyFileDirector(imgBatchCompressAddImgExWaterResult);
        copyFileDirector.construct();
    }

    @Override
    public
    void imgBatchCompressCut() {
        CopyFileDirector copyFileDirector = new CopyFileDirector(imageBatchCompressCutResult);
        copyFileDirector.construct();
    }

    @Override
    public
    void imgBatchCompressSize() {
        CopyFileDirector copyFileDirector = new CopyFileDirector(imageBatchCompressResult);
        copyFileDirector.construct();
    }

}
