package com.my.tool.utils.search.handler;

import com.my.tool.utils.search.service.ClientService;
import com.my.tool.utils.search.utils.FileUtil;
import com.my.tool.utils.search.utils.ThumbnailatorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ImgBatchCompressCutHandler implements Handler {

    final String  FUNCTION_NAME = "cut";

    @Autowired
    private ClientService clientService;

    @Override
    public void deal(Chain chain) {
        String type = chain.request();
        if (FUNCTION_NAME.equals(type)){
            clientService.imgBatchCompressCut();
        }else {
            chain.proceed(type);
        }
    }
}
