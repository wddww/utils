package com.my.tool.utils.search.builder;

import com.my.tool.utils.search.model.RegularSearchConfig;
import com.my.tool.utils.search.utils.FileToZip;
import com.my.tool.utils.search.utils.FileUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@Component
@Slf4j
public class RemoveResult extends CopyFileBuilder {

    @Autowired
    RegularSearchConfig config;

    @Autowired
    private FileUtil fileUtil;

    /**
     * 从哪个文件夹下开始查找
     * @return 返回查找到的结果
     */
    @Override
    public  List <File>  getResource(){
        return fileUtil.getFileList( config.getResourceRegularPath(), false );
    }

    @Override
    public List <File>  searchFile() {
        return null;
    }


    @Override
    public void getResult() {
        String pattern = config.getRegular();  // 字符串中是否包含了 '原片' 子字符串
        long beginTime = System.currentTimeMillis();
        fileUtil.findFileByRegularAndCopyFile(config.getResourceRegularPath(),pattern,0,0,config.getCopyRegularPath());
        long endTime = System.currentTimeMillis();
        log.info("There are " + fileUtil.getAll() +" resource pools and target file find : " + fileUtil.getSort() + ", consuming time " + (endTime - beginTime) / 1000 + " Ms. ");
    }


    @Override
    public void creatFolder() {
        try{
            File file = new File( this.config.getCopyRegularPath());
            if(fileUtil.checkFolderExist(file)){
                file.mkdir();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }



    @Override
    public void removeFiles() {
        try{
            File file = new File( this.config.getCopyRegularPath() );
            if(fileUtil.checkFileExist(file)){
                fileUtil.deleteFile( file );
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
