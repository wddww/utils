package com.my.tool.utils.search.thread;

public class MyRunnable implements Runnable {

    static int j = 100;

    @Override
    public synchronized void run() {
        long l1 = System.currentTimeMillis();
        for (int i=0;i<100;i++){
                System.out.println(j--);
        }
        long l2 = System.currentTimeMillis();
        System.out.println(Thread.currentThread().getName()+ "时间差 "+ (l2 - l1));
    }

    public static void main(String[] args) {
        MyRunnable myThread = new MyRunnable();
        Thread t1 = new Thread(myThread);
        Thread t2 = new Thread(myThread);
        Thread t3 = new Thread(myThread);
        t1.start();
        t2.start();
        t3.start();
    }
}
