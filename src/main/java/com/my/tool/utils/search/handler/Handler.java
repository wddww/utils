package com.my.tool.utils.search.handler;

public interface  Handler  {

    public  void deal(Chain chain);


    interface Chain{

         String request();

         void  proceed(String request);
    }



}
