package com.my.tool.utils.entity;

import net.coobird.thumbnailator.geometry.Position;

/**
 * @ClassName DingdPhotoUtil
 * @Description TODO 图片处理工具抽象类
 * @Author wangdong_02
 * Date 2022/3/9 9:37
 * @Version 1.0
 **/
public abstract class  DingdPhotoUtil {

    /**
     * @Author wangdong_02
     * @Description //TODO  图片尺寸不变，压缩图片文件大小
     * @Date 9:44 2022/3/9
     * @Param
     * @return
     **/
    public  abstract  byte[] compressImage();

    /**
     * @Author wangdong_02
     * @Description //TODO  添加图片水印
     * @Date 9:54 2022/3/9
     * @Param 
     * @return 
     **/
    public abstract byte[] addImageWater();


}