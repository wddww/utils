@ECHO OFF
@REM 
echo 办公小工具

:: 默认为在该执行文件的当前路径 （可以自定义）
set ROOT_PATH=%~dp0

set workspace="bin"

:MENU
echo. =======================================================================
	echo.  [1] 根据文件名搜索文件（会移动文件）
	echo.  [2] 正则表达式搜索文件（会压缩ZIP）
	echo.  [3] 正则表达式搜索文件（会移动文件）
	echo.  [4] 正则表达式搜索文件（只打印文件）
	echo.  [5] 测试Python
	echo.  [6] 图片原图压缩（会移动文件）
	echo.  [7] 图片裁剪（会移动文件）
	echo.  [8] 图片加文字水印（会移动文件）
	echo.  [0] 退 出          
echo. =======================================================================          


echo.请输入选择的序号:
set /p ID=
	IF "%id%"=="1" GOTO deepLeveSearchDoc 
	IF "%id%"=="2" GOTO regularSearchDoc
	IF "%id%"=="3" GOTO regularSearchToCopyDoc
	IF "%id%"=="4" GOTO regularSearchFile
	IF "%id%"=="5" GOTO testPython
	IF "%id%"=="6" GOTO imgdeal
	IF "%id%"=="7" GOTO imgcut
	IF "%id%"=="8" GOTO imgaddwater
	IF "%id%"=="0" EXIT
PAUSE 
GOTO MENU


::*************************************************************************************************************
::开始搜索文件  
:deepLeveSearchDoc
	call %ROOT_PATH%%workspace%\deep_level_copy.bat %ROOT_PATH%
	echo 已执行完毕！
	GOTO MENU

:regularSearchDoc
    call %ROOT_PATH%bin\regular_zip.bat %ROOT_PATH%
    echo 已执行完毕！
    GOTO MENU
	
:regularSearchToCopyDoc
    call %ROOT_PATH%bin\regular_copy.bat %ROOT_PATH%
    echo 已执行完毕！
    GOTO MENU
	
:regularSearchFile
    call %ROOT_PATH%bin\search_file.bat %ROOT_PATH%
    echo 已执行完毕！
    GOTO MENU
	
:imgdeal
    call %ROOT_PATH%bin\imgdeal.bat %ROOT_PATH%
    echo 已执行完毕！
    GOTO MENU

:testPython
    call %ROOT_PATH%bin\python_test.bat %ROOT_PATH%
    echo 已执行完毕！
    GOTO MENU
	
:imgcut
    call %ROOT_PATH%bin\imgcut.bat %ROOT_PATH%
    echo 已执行完毕！
    GOTO MENU
	
:imgaddwater
    call %ROOT_PATH%bin\imgaddwater.bat %ROOT_PATH%
    echo 已执行完毕！
    GOTO MENU
 	












    







